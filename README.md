# SSHELP

[[_TOC_]]

## Help

```
USAGE:
    sshelp [OPTIONS] [SUBCOMMAND]

OPTIONS:
    -h, --help                 Print help information
    -i, --index <INDEX>        [default: 0]
    -n, --name <NAME>
    -p, --profile <PROFILE>    [default: default]
    -r, --region <REGION>      [default: eu-west-1]
    -V, --version              Print version information

SUBCOMMANDS:
    help    Print this message or the help of the given subcommand(s)
    ssh
    ssm
```

## Use cases

### Account wide

```
❯ sshelp -p $PROFILE
|  # |                  ID |       Public IP |      Private IP | Name |
|  1 | i-XXXXXXXXXXXXXXXXX | AAA.BBB.CCC.DDD | EEE.FFF.GGG.HHH |  foo |
|  2 | i-YYYYYYYYYYYYYYYYY | SSS.TTT.UUU.VVV | WWW.XXX.YYY.ZZZ |  bar |
```

### Perfect match

```
❯ sshelp -p $PROFILE -n foo
|  1 | i-XXXXXXXXXXXXXXXXX | AAA.BBB.CCC.DDD | EEE.FFF.GGG.HHH |  foo |
```

### Wildcard

```
❯ sshelp -p $PROFILE -n fo*
|  1 | i-XXXXXXXXXXXXXXXXX | AAA.BBB.CCC.DDD | EEE.FFF.GGG.HHH |  foo |
```

### `SSH` et `SSM`

```
❯ sshelp -p $PROFILE -n foo ssh
AAA.BBB.CCC.DDD
```

```
❯ sshelp -p $PROFILE -n foo ssm
i-XXXXXXXXXXXXXXXXX
```

## Integration

Both the following integration expect an octothorpe separated input : `ssh name#index`.
On invocation, `ssh` will work the following steps:
1. Match the `hostname`,
2. Split on `#` and use `0` ans the default index if missing,
3. Call `sshelp` to retrieve the needed `IP` or instance indentifier,
4. Carry on with the connexion process.

> It'd be trivial to create an `ssh name#profile#index` version if needs be.

### SSH via bastion

```
Host bastion
    Hostname bastion.tld

Host $NAME*
    ProxyCommand ssh -W $(sshelp --profile __PROFILE__ --name $(echo %h | awk -F# '{print $1}') --index $(echo %h | awk -F# '{print ($2==""?0:$2)}') ssh):%p $BASTION
```

### SSM

```
Host $NAME*
    ProxyCommand sh -c "aws --profile __PROFILE__ ssm start-session --target $(sshelp --profile __PROFILE__ --name $(echo %h | awk -F# '{print $1}') --index $(echo %h | awk -F# '{print ($2==""?0:$2)}') ssm) --document-name AWS-StartSSHSession --parameters 'portNumber=%p'"
```
