use anyhow::Result;
use aws_sdk_ec2::operation::describe_instances::DescribeInstancesOutput;
use aws_sdk_ec2::types::{Filter, Instance};
use aws_sdk_ec2::Client;

#[derive(Default)]
pub struct DescribeInstances {
    pub name: Option<String>,
}

struct Filters(Option<Vec<Filter>>);

impl From<DescribeInstances> for Filters {
    fn from(describe_instances: DescribeInstances) -> Self {
        let mut filters = vec![Filter::builder()
            .set_name(Some("instance-state-name".to_owned()))
            .set_values(Some(vec!["running".to_owned()]))
            .build()];

        if let Some(name) = describe_instances.name {
            filters.push(
                Filter::builder()
                    .set_name(Some("tag:Name".to_owned()))
                    .set_values(Some(vec![name]))
                    .build(),
            )
        }

        Self(Some(filters))
    }
}

pub struct Instances(DescribeInstancesOutput);

impl Instances {
    pub async fn new(client: &Client, describe_instances: DescribeInstances) -> Result<Self> {
        Ok(Self(
            client
                .describe_instances()
                .set_filters(Filters::from(describe_instances).0)
                .send()
                .await?,
        ))
    }

    fn instance(&self, mut index: usize) -> Option<&Instance> {
        if let Some(reservations) = self.0.reservations() {
            for reservation in reservations {
                if let Some(instances) = reservation.instances() {
                    for instance in instances {
                        if index == 0 {
                            return Some(instance);
                        } else {
                            index -= 1;
                        }
                    }
                }
            }
        }
        None
    }

    pub fn ssh(&self, index: usize) -> Option<&str> {
        self.instance(index)
            .map(|instance| instance.private_ip_address())
            .unwrap()
    }

    pub fn ssm(&self, index: usize) -> Option<&str> {
        self.instance(index)
            .map(|instance| instance.instance_id())
            .unwrap()
    }
}

impl std::fmt::Display for Instances {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if let Some(reservations) = self.0.reservations() {
            let (
                mut id_length,
                mut name_length,
                mut public_length,
                mut private_length,
                mut cardinal,
            ) = (0, 0, 0, 0, 0);
            for reservation in reservations {
                if let Some(instances) = reservation.instances() {
                    for instance in instances {
                        let len = instance.instance_id().map(|s| s.len()).unwrap_or(0);
                        if len > id_length {
                            id_length = len;
                        }

                        if let Some(tags) = instance.tags() {
                            if let Some(tag) = tags.iter().find(|t| t.key() == Some("Name")) {
                                let len = tag.value().map(|s| s.len()).unwrap_or(0);
                                if len > name_length {
                                    name_length = len;
                                }
                            }
                        }

                        let len = instance.public_ip_address().map(|s| s.len()).unwrap_or(0);
                        if len > public_length {
                            public_length = len;
                        }

                        let len = instance.private_ip_address().map(|s| s.len()).unwrap_or(0);
                        if len > private_length {
                            private_length = len;
                        }

                        cardinal += 1;
                    }
                }
            }
            let cardinal = cardinal.to_string().len();
            if public_length == 0 {
                public_length = 15;
            }
            if private_length == 0 {
                private_length = 15;
            }

            writeln!(
                f,
                "| {:>cardinal$} | {:>id_length$} | {:>public_length$} | {:>private_length$} | {:>name_length$} |",
                "#", "ID", "Public IP", "Private IP", "Name"
            )?;
            let mut index = 1;
            for reservation in reservations {
                if let Some(instances) = reservation.instances() {
                    for instance in instances {
                        let name = {
                            if let Some(tags) = instance.tags() {
                                if let Some(tag) = tags.iter().find(|t| t.key() == Some("Name")) {
                                    tag.value()
                                } else {
                                    None
                                }
                            } else {
                                None
                            }
                        };

                        writeln!(
                            f,
                            "| {:>cardinal$} | {:>id_length$} | {:>public_length$} | {:>private_length$} | {:>name_length$} |",
                            index,
                            instance.instance_id().unwrap(),
                            instance.public_ip_address().unwrap_or(""),
                            instance.private_ip_address().unwrap_or(""),
                            name.unwrap_or("")
                        )?;
                        index += 1;
                    }
                }
            }
        } else {
            writeln!(f, "No reservation found")?;
        }

        Ok(())
    }
}
