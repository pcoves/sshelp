mod instance;
mod options;

use crate::{
    instance::{DescribeInstances, Instances},
    options::{Command, Options},
};
use anyhow::Result;
use aws_sdk_ec2::Client;
use clap::Parser;

#[tokio::main]
async fn main() -> Result<()> {
    let options = Options::parse();

    std::env::set_var("AWS_PROFILE", options.profile);
    std::env::set_var("AWS_REGION", options.region);

    let client = Client::new(&aws_config::load_from_env().await);
    let instances = Instances::new(&client, DescribeInstances { name: options.name }).await?;

    match options.command {
        None => println!("{instances}"),
        Some(command) => match command {
            Command::Ssh => println!("{}", instances.ssh(options.index).unwrap_or("")),
            Command::Ssm => println!("{}", instances.ssm(options.index).unwrap_or("")),
        },
    }

    Ok(())
}
