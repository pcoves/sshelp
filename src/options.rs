use clap::{Parser, Subcommand};

#[derive(Debug, Parser)]
#[clap(author, version, about, long_about = None)]
pub struct Options {
    #[clap(short, long, default_value = "eu-west-1")]
    pub region: String,

    #[clap(short, long, default_value = "default")]
    pub profile: String,

    #[clap(short, long)]
    pub name: Option<String>,

    #[clap(subcommand)]
    pub command: Option<Command>,

    #[clap(short, long, default_value_t = 0)]
    pub index: usize,
}

#[derive(Debug, Subcommand)]
pub enum Command {
    Ssh,
    Ssm,
}
